package com.k.mapsearch.API;

import com.k.mapsearch.PlaceDataModel.JSONResult;
import com.k.mapsearch.RouteDataModel.RouteResponse;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Query;

public class Retrofit {

    private static final String ENDPOINT = "https://maps.googleapis.com/maps/api";
    private static ApiInterface apiInterface;

    public static final String API_KEY = "AIzaSyDviI42XVE6KJPtF4vz6TANYsGrz83OJ4c";


    interface ApiInterface {
        @GET("/place/nearbysearch/json")
        void getPlacesByType(@Query("location") String location,
                             @Query("types") String types,
                             @Query("radius") String radius,
                             @Query("key") String key,
                             Callback<JSONResult> callback);

        @GET("/place/textsearch/json")
        void getPlacesByQuery(@Query("location") String location,
                              @Query("query") String types,
                              @Query("radius") String radius,
                              @Query("language") String language,
                              @Query("key") String key,
                              Callback<JSONResult> callback);


        @GET("/directions/json")
        void getRoute(@Query(value = "origin", encodeValue = false) String origin,
                      @Query(value = "destination", encodeValue = false) String destination,
                      @Query("language") String language,
                      Callback<RouteResponse> callback);
    }

    static {
        initialize();
    }

    public static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getPlacesByType(double lat, double lng, String type, String radius, Callback<JSONResult> callback) {
        String strLocation = String.valueOf(lat) + ',' + String.valueOf(lng);
        apiInterface.getPlacesByType(strLocation,
                type,
                String.valueOf(radius),
                API_KEY,
                callback);
    }

    public static void getPlacesByQuery(double lat, double lng, String query, String radius, Callback<JSONResult> callback) {
        String strLocation = String.valueOf(lat) + ',' + String.valueOf(lng);
        apiInterface.getPlacesByQuery(strLocation,
                query,
                String.valueOf(radius),
                "ru",
                API_KEY,
                callback);
    }

    public static void getRoute(String origin, String destination, Callback<RouteResponse> callback) {
        apiInterface.getRoute(origin,
                destination,
                "ru",
                callback);
    }
}
