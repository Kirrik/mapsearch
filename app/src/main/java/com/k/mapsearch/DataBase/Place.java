package com.k.mapsearch.DataBase;

public class Place {

    public double lattitude;
    public double longitude;
    public String name;

    public Place() {

    }

    public Place(double lattitude, double longitude, String name) {
        this.lattitude = lattitude;
        this.longitude = longitude;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Place{" +
                "lattitude=" + lattitude +
                ", longitude=" + longitude +
                ", name='" + name + '\'' +
                '}';
    }
}
