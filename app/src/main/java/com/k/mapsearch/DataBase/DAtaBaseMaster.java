package com.k.mapsearch.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import static com.k.mapsearch.DataBase.DataBaseCreator.PlacesColumns.LATITUDE;
import static com.k.mapsearch.DataBase.DataBaseCreator.PlacesColumns.LONGITUDE;
import static com.k.mapsearch.DataBase.DataBaseCreator.PlacesColumns.NAME;
import static com.k.mapsearch.DataBase.DataBaseCreator.PlacesColumns.TABLE_NAME;

public class DataBaseMaster {

    private SQLiteDatabase database;
    private DataBaseCreator dbCreator;

    private static DataBaseMaster instance;

    private DataBaseMaster(Context context) {
        dbCreator = new DataBaseCreator(context);
        if (database == null || !database.isOpen()) {
            database = dbCreator.getWritableDatabase();
        }
    }

    public static DataBaseMaster getInstance(Context context) {
        if (instance == null) {
            instance = new DataBaseMaster(context);
        }
        return instance;
    }


    public void addPlace(Place place) {
        ContentValues cv = new ContentValues();
        cv.put(LATITUDE, place.lattitude);
        cv.put(LONGITUDE, place.longitude);
        cv.put(NAME, place.name);
        database.insert(TABLE_NAME, null, cv);
    }

    @Nullable
    public List<Place> getAllPlaces() {
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = database.rawQuery(query, null);

        List<Place> _list = new ArrayList<>();

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Place place = new Place();
                place.lattitude = cursor.getDouble(cursor.getColumnIndex(LATITUDE));
                place.longitude = cursor.getDouble(cursor.getColumnIndex(LONGITUDE));
                place.name = cursor.getString(cursor.getColumnIndex(NAME));
                _list.add(place);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return _list;
    }

    public int clearDataBase() {
        return database.delete(TABLE_NAME, null, null);
    }

}
