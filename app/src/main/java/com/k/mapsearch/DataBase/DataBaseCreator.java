package com.k.mapsearch.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;


public class DataBaseCreator extends SQLiteOpenHelper {


    public static final String DB_NAME = "MapSearchDb";
    public static final int DB_VERSION = 1;

    public static class PlacesColumns implements BaseColumns {
        public static final String TABLE_NAME = "tbl_places";
        public static final String LATITUDE = "lattitude";
        public static final String LONGITUDE = "longitude";
        public static final String NAME = "name";
    }

    private static String CREATE_TABLE_PLACES =
            "CREATE TABLE " + PlacesColumns.TABLE_NAME +
                    " (" +
                    PlacesColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    PlacesColumns.LATITUDE + " REAL, " +
                    PlacesColumns.LONGITUDE + " REAL, " +
                    PlacesColumns.NAME + " TEXT" +
                    ");";

    public DataBaseCreator(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_PLACES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
