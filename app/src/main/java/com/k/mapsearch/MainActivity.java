package com.k.mapsearch;

import android.app.Activity;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.k.mapsearch.API.Retrofit;
import com.k.mapsearch.DataBase.DataBaseMaster;
import com.k.mapsearch.DataBase.Place;
import com.k.mapsearch.PlaceDataModel.JSONResult;
import com.k.mapsearch.RouteDataModel.RouteResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends FragmentActivity implements TextView.OnEditorActionListener {

    public static final String RADIUS = "5000";

    GoogleMap map;
    Marker tempMarker;
    Marker markerEndPoint;
    Polyline polyline;
    private LocationManager locationManager;
    JSONResult lastJSONResult;
    private double currentLatitude = 0.0;
    private double currentLongitude = 0.0;
    DataBaseMaster db;

    EditText etSearch;
    Button bClear;
    Button bChangeMapType;
    Button bCurrPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //получение БД
        db = DataBaseMaster.getInstance(getApplicationContext());

        etSearch = (EditText) findViewById(R.id.et_search);
        etSearch.setOnEditorActionListener(this);

        bClear = (Button) findViewById(R.id.b_clear);
        bChangeMapType = (Button) findViewById(R.id.b_change_map_type);
        bCurrPosition = (Button) findViewById(R.id.b_curr_position);

        map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_view)).getMap();
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 20));
        map.animateCamera(CameraUpdateFactory.zoomTo(2), 2000, null);


        //при нажатии на карту - погашение клавиатуры
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS, 0);
            }
        });

        //когда пользователь нажимает на маркер, обратиться к предыдущему(нажатому до этого) нет возможности, кроме как хранить его отдельно.
        //tempMarker - сохраненный маркер, активный на данный момент. когда пользователь выберет другой, tempMarker надо будет перекрасить обратно в родной цвет
        //markerEndPoint - маркер конца маршрута. его нужно сохранить отдельно, чтобы при построении нового маршрута удалить данные о старом.
        //к сожалению, строить маршрут именно к конкретному маркеру не вышло. маркеры маршрута и пара откуда+куда не совпадают - странная работа гугла.
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (tempMarker != null) {
                    tempMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                    markerEndPoint.remove();
                }
                tempMarker = marker;
                //красим выбранный пользователем маркер в синий цвет
                marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                marker.showInfoWindow();

                //запрос на получение маршрута
                String origin = String.valueOf(currentLatitude) + "," + String.valueOf(currentLongitude);
                String destination = String.valueOf(marker.getPosition().latitude) + "," + String.valueOf(marker.getPosition().longitude);
                Retrofit.getRoute(origin, destination, new Callback<RouteResponse>() {
                    @Override
                    public void success(RouteResponse routeResponse, Response response) {
                        //прорисовка маршрута
                        drawRoute(routeResponse);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(getApplicationContext(), "Маршрут не построен.", Toast.LENGTH_SHORT).show();
                    }
                });
                return true;
            }
        });

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        //очистка поля ввода и всех маркеров с карты
        bClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSearch.setText("");
                map.clear();
            }
        });

        //смена типа карты
        bChangeMapType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (map.getMapType() == GoogleMap.MAP_TYPE_NORMAL) {
                    map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                } else if (map.getMapType() == GoogleMap.MAP_TYPE_HYBRID) {
                    map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                }
            }
        });

        //получение текущей позиции и установка туда маркера
        bCurrPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 15));
                map.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
                map.addMarker(new MarkerOptions().position(new LatLng(currentLatitude, currentLongitude))
                        .title("You are here")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000 * 10, 10, locationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000 * 10, 10, locationListener);

        if (lastJSONResult == null) {
            selectFromDBLastState();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //отключение слушателя геолокации
        locationManager.removeUpdates(locationListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //сохранение в бд текущего состояния маркеров перед закрытием
        saveTheCurentStateToDB();
    }

    private LocationListener locationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            //обновлениетекущего местоположения
            if (currentLatitude == 0.0 && currentLongitude == 0.0) {
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15));
                map.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
            }
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
        }

        @Override
        public void onProviderDisabled(String provider) {

        }

        @Override
        public void onProviderEnabled(String provider) {
            currentLatitude = locationManager.getLastKnownLocation(provider).getLatitude();
            currentLongitude = locationManager.getLastKnownLocation(provider).getLongitude();
            map.addMarker(new MarkerOptions().position(new LatLng(currentLatitude, currentLongitude))
                    .title("You are here")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }
    };

    //кнопка поиска на виртуальной клавиатуре
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            //запрос на получение мест по ТИПУ! Я так понял задание.
            Retrofit.getPlacesByType(currentLatitude, currentLongitude, etSearch.getText().toString(), RADIUS, new Callback<JSONResult>() {
                @Override
                public void success(JSONResult jsonResults, Response response) {
                    if (jsonResults.status == "OK") {
                        //сохраняем отдельно полученный ответ и проставляем новые маркеры
                        lastJSONResult = jsonResults;
                        addMarkersToMap(jsonResults);
                    } else {
                        //для удобства тестирования приложения, если в поле для ввода ввести "clear", очистится база данных с сохраненными маркерами.
                        if (etSearch.getText().toString().equalsIgnoreCase("clear")) {
                            db.clearDataBase();
                            Toast.makeText(getApplicationContext(), "DataBase cleared!", Toast.LENGTH_SHORT).show();
                        } else {
                            //Если по типу не нашли, ищем тексту
                            Retrofit.getPlacesByQuery(currentLatitude, currentLongitude, etSearch.getText().toString(), RADIUS, new Callback<JSONResult>() {
                                @Override
                                public void success(JSONResult jsonResults, Response response) {
                                    //сохраняем отдельно полученный ответ и проставляем новые маркеры
                                    lastJSONResult = jsonResults;
                                    addMarkersToMap(jsonResults);
                                }

                                @Override
                                public void failure(RetrofitError error) {

                                }
                            });
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
            return true;
        }
        return false;
    }

    //добавление новых маркеров на карту
    private void addMarkersToMap(JSONResult jsonResults) {
        map.clear();
        for (int i = 0; i < jsonResults.results.length; i++) {
            double lat = jsonResults.results[i].geometry.location.lat;
            double lng = jsonResults.results[i].geometry.location.lng;
            map.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title(jsonResults.results[i].name));
        }
    }

    //прорисовка маршрута
    private void drawRoute(RouteResponse routeResponse) {
        List<LatLng> mPoints = PolyUtil.decode(routeResponse.getPoints());
        PolylineOptions line = new PolylineOptions();
        line.width(9f);
        LatLngBounds.Builder latLngBuilder = new LatLngBounds.Builder();
        for (int i = 0; i < mPoints.size(); i++) {
            if (i == 0) {
                MarkerOptions startMarkerOptions = new MarkerOptions()
                        .position(mPoints.get(i))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                map.addMarker(startMarkerOptions);
            } else if (i == mPoints.size() - 1) {
                MarkerOptions endMarkerOptions = new MarkerOptions()
                        .position(mPoints.get(i))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                markerEndPoint = map.addMarker(endMarkerOptions);
            }
            line.add(mPoints.get(i));
            latLngBuilder.include(mPoints.get(i));
        }
        if (polyline != null) {
            polyline.setPoints(new ArrayList<LatLng>());

        }
        polyline = map.addPolyline(line);
        int size = getResources().getDisplayMetrics().widthPixels;
        LatLngBounds latLngBounds = latLngBuilder.build();
        CameraUpdate track = CameraUpdateFactory.newLatLngBounds(latLngBounds, size, size, 25);
        map.moveCamera(track);
    }

    //сохранение текущего состояния маркеров на карте
    private void saveTheCurentStateToDB() {
        if (lastJSONResult != null && lastJSONResult.results.length > 0) {
            db.clearDataBase();
            for (int i = 0; i < lastJSONResult.results.length; i++) {
                double lat = lastJSONResult.results[i].geometry.location.lat;
                double lng = lastJSONResult.results[i].geometry.location.lng;
                map.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title(lastJSONResult.results[i].name));
                Place place = new Place(lat, lng, lastJSONResult.results[i].name);

                db.addPlace(place);
            }
        }
    }

    //сохранение сохраненного состояния маркеров из базы
    private void selectFromDBLastState() {
        List<Place> _list = db.getAllPlaces();
        if (_list != null && _list.size() > 0) {
            int i = 0;
            for (Place place : _list) {
                double lat = place.lattitude;
                double lng = place.longitude;
                map.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title(place.name));
            }
        }
    }

}